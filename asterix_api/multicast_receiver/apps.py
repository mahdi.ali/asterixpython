from django.apps import AppConfig


class MulticastReceiverConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'multicast_receiver'

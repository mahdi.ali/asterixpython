import psycopg2

# Connect to Dev DB
con = psycopg2.connect(
		host = "103.41.204.105",
		database = "atx_db",
		user = "postgres",
		password = "MMS@P@ssw0rd123",
		port = 5432)

def testDBSelect():
	print("TEST DB SELECT")
	cur = con.cursor()

	cur.execute("SELECT location_name, city FROM location")

	rows = cur.fetchall()

	for r in rows:
		print(f"Location Name: {r[0]}")
		printf(f"City        : {r[1]}")

	cur.close()

	# Close DB
	con.close()

from django.urls import path
from . import views

app_name = 'parser'

urlpatterns = [
	path('testDBSelect', views.testDBSelect, name = 'select'),
]
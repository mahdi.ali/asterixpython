from django.urls import path
from . import views

app_name = 'api'

urlpatterns = [
	path('dummy_endpoint', views.my_json_api, name = 'dummy'),
	path('static_pcap', views.static_decode, name = 'static'),
	path('db_test', views.dummy_db_select, name = 'db_test'),
	path('decode_file/<str:file_name>', views.decode_file, name = "decode_file")
]
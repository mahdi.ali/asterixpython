from django.shortcuts import render
from django.http import JsonResponse
import json

from . import process
from . import db

# Dummy JSON response
def my_json_api(request):
	data = {
		'message':'Hello from JSON API!'
	}
	return JsonResponse(data)

# Returns JSON response of decoded PCAP file
def static_decode(request):
	data = process.decoder_example()
	return JsonResponse(data, safe = False)

def decode_file(request, file_name):
	data = process.decode_file(file_name)
	return JsonResponse(data, safe = False)

# Decoding Asterix byte array
def decode_byte_array(request):

	if request.method == 'POST':
		# Get raw payload (bytes)
		raw_body = request.body

		# Decode raw data to a dictionary
		try:
			body = json.loads(raw_body)
		except json.JSONDecodeError:
			return JsonResponse({'error':'Invalid JSON data'}, status=400)

		data = process.decode_byte_array(body.get('payload'))

def dummy_db_select(request):
	return JsonResponse({'data':db.testDBSelect()})
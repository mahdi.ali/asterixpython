__author__ = 'mahdi.ali'

import asterix
import os
import dpkt
import base64

def decoder_example():
    cur_path = os.path.dirname(__file__)
    with open (cur_path + "/static/asterix_data/record02aug.pcap", "rb") as f:
        pcap = dpkt.pcap.Reader(f)

        cntr = 1
        for ts, buf in pcap:
            eth = dpkt.ethernet.Ethernet(buf)
            data = eth.ip.udp.data

            hexdata = ":".join("{:02x}".format(ord(c)) for c in str(data))
            print('Parsing packet %d : %s' % (cntr, hexdata))
            cntr += 1

            # Parse data
            parsed = asterix.parse(data)

            return parsed

# Decoding asterix byte array
def decode_byte_array(asterix_packet):

    # Parse and print packet
    parsed = asterix.parse(asterix_packet)

    # Describe Asterix data
    formatted = asterix.describe(parsed)

    return formatted

def decode_file(file_name):
    cur_path = os.path.dirname(__file__)
    with open (cur_path + "/static/asterix_data/" + file_name, "rb") as f:
        pcap = dpkt.pcap.Reader(f)

        cntr = 1
        for ts, buf in pcap:
            eth = dpkt.ethernet.Ethernet(buf)
            data = eth.ip.udp.data

            hexdata = ":".join("{:02x}".format(ord(c)) for c in str(data))
            print('Parsing packet %d : %s' % (cntr, hexdata))
            cntr += 1

            # Parse data
            parsed = asterix.parse(data)

            return parsed

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AsterixPacket(models.Model):
    category = models.IntegerField(blank=True, null=True)
    len = models.IntegerField(blank=True, null=True)
    crc = models.CharField(max_length=-1, blank=True, null=True)
    ts = models.FloatField(blank=True, null=True)
    hexdata = models.TextField(blank=True, null=True)
    i010_sac = models.IntegerField(blank=True, null=True)
    i010_sic = models.IntegerField(blank=True, null=True)
    i015_id = models.IntegerField(blank=True, null=True)
    i016_rp = models.FloatField(blank=True, null=True)
    i020_ecat = models.IntegerField(blank=True, null=True)
    i040_atp = models.IntegerField(blank=True, null=True)
    i040_arc = models.IntegerField(blank=True, null=True)
    i040_rc = models.IntegerField(blank=True, null=True)
    i040_rab = models.IntegerField(blank=True, null=True)
    i040_fx = models.IntegerField(blank=True, null=True)
    i040_dcr = models.IntegerField(blank=True, null=True)
    i040_gbs = models.IntegerField(blank=True, null=True)
    i040_sim = models.IntegerField(blank=True, null=True)
    i040_tst = models.IntegerField(blank=True, null=True)
    i040_saa = models.IntegerField(blank=True, null=True)
    i040_cl = models.IntegerField(blank=True, null=True)
    i040_spare = models.IntegerField(blank=True, null=True)
    i040_llc = models.IntegerField(blank=True, null=True)
    i040_ipc = models.IntegerField(blank=True, null=True)
    i040_nogo = models.IntegerField(blank=True, null=True)
    i040_cpr = models.IntegerField(blank=True, null=True)
    i040_ldpj = models.IntegerField(blank=True, null=True)
    i040_rcf = models.IntegerField(blank=True, null=True)
    i073_time_reception_position = models.FloatField(blank=True, null=True)
    i075_time_reception_velocity = models.FloatField(blank=True, null=True)
    i077_time_report_transmission = models.FloatField(blank=True, null=True)
    i080_taddr = models.CharField(max_length=-1, blank=True, null=True)
    i090_nucr_or_nacv = models.IntegerField(blank=True, null=True)
    i090_nucp_or_nic = models.IntegerField(blank=True, null=True)
    i090_fx = models.IntegerField(blank=True, null=True)
    i090_nicbaro = models.IntegerField(blank=True, null=True)
    i090_sil = models.IntegerField(blank=True, null=True)
    i090_nacp = models.IntegerField(blank=True, null=True)
    i090_spare = models.IntegerField(blank=True, null=True)
    i090_sils = models.IntegerField(blank=True, null=True)
    i090_sda = models.IntegerField(blank=True, null=True)
    i090_gva = models.IntegerField(blank=True, null=True)
    i090_pic = models.IntegerField(blank=True, null=True)
    i130_lat = models.FloatField(blank=True, null=True)
    i130_lon = models.FloatField(blank=True, null=True)
    i131_lat = models.FloatField(blank=True, null=True)
    i131_lon = models.FloatField(blank=True, null=True)
    i132_mam = models.FloatField(blank=True, null=True)
    i140_geometric_height = models.FloatField(blank=True, null=True)
    i145_fl = models.FloatField(blank=True, null=True)
    i157_re = models.IntegerField(blank=True, null=True)
    i157_gvr = models.FloatField(blank=True, null=True)
    i160_re = models.IntegerField(blank=True, null=True)
    i160_gs = models.FloatField(blank=True, null=True)
    i160_ta = models.FloatField(blank=True, null=True)
    i161_spare = models.IntegerField(blank=True, null=True)
    i161_trackn = models.IntegerField(blank=True, null=True)
    i170_tid = models.CharField(max_length=-1, blank=True, null=True)
    i200_icf = models.IntegerField(blank=True, null=True)
    i200_lnav = models.IntegerField(blank=True, null=True)
    i200_me = models.IntegerField(blank=True, null=True)
    i200_ps = models.IntegerField(blank=True, null=True)
    i200_ss = models.IntegerField(blank=True, null=True)
    i210_spare = models.IntegerField(blank=True, null=True)
    i210_vns = models.IntegerField(blank=True, null=True)
    i210_vn = models.IntegerField(blank=True, null=True)
    i210_ltt = models.IntegerField(blank=True, null=True)
    i295_trd_trd = models.FloatField(blank=True, null=True)
    i295_qi_qi = models.FloatField(blank=True, null=True)
    i295_mam_mam = models.FloatField(blank=True, null=True)
    i295_gh_gh = models.FloatField(blank=True, null=True)
    i295_fl_fl = models.FloatField(blank=True, null=True)
    i295_gvr_gvr = models.FloatField(blank=True, null=True)
    i295_gv_gv = models.FloatField(blank=True, null=True)
    i295_tid_tid = models.FloatField(blank=True, null=True)
    i295_ts_ts = models.FloatField(blank=True, null=True)
    i400_rid = models.IntegerField(blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'asterix_packet'


class CnsMaster(models.Model):
    device_group_id = models.CharField(primary_key=True, max_length=10)
    device_group_desc = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cns_master'


class ConfigApp(models.Model):
    ip_address = models.CharField(max_length=255, blank=True, null=True)
    port = models.IntegerField(blank=True, null=True)
    pool_size = models.IntegerField(blank=True, null=True)
    id = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'config_app'


class Device(models.Model):
    device_id = models.CharField(primary_key=True, max_length=20)
    device_cat_id = models.CharField(max_length=20, blank=True, null=True)
    device_group_id = models.CharField(max_length=2, blank=True, null=True)
    system_id = models.CharField(max_length=50, blank=True, null=True)
    device_name = models.CharField(max_length=100, blank=True, null=True)
    vendor_id = models.CharField(max_length=50, blank=True, null=True)
    location_id = models.CharField(max_length=50, blank=True, null=True)
    status_id = models.CharField(max_length=50, blank=True, null=True)
    status_date = models.DateTimeField(blank=True, null=True)
    regional_id = models.CharField(max_length=1, blank=True, null=True)
    otban_id = models.CharField(max_length=20, blank=True, null=True)
    coordinate_id = models.CharField(max_length=100, blank=True, null=True)
    cluster_id = models.CharField(max_length=100, blank=True, null=True)
    alamat = models.CharField(max_length=200, blank=True, null=True)
    latitude = models.TextField(blank=True, null=True)
    longitude = models.TextField(blank=True, null=True)
    error_desc = models.CharField(max_length=100, blank=True, null=True)
    ip_address = models.CharField(max_length=50, blank=True, null=True)
    altitude = models.TextField(blank=True, null=True)
    speed = models.TextField(blank=True, null=True)
    origin_port = models.TextField(blank=True, null=True)
    destination_port = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=-1, blank=True, null=True)
    break_status = models.TextField(blank=True, null=True)
    flight_number = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'device'


class DeviceCat(models.Model):
    device_cat_id = models.CharField(primary_key=True, max_length=20)
    device_cat_desc = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'device_cat'


class DeviceTracking(models.Model):
    flight_number = models.TextField(blank=True, null=True)
    device_id = models.CharField(max_length=20, blank=True, null=True)
    latitude = models.TextField(blank=True, null=True)
    longitude = models.TextField(blank=True, null=True)
    status_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'device_tracking'


class InspectureNote(models.Model):
    note_seq = models.DecimalField(primary_key=True, max_digits=65535, decimal_places=65535)
    device_id = models.CharField(max_length=100, blank=True, null=True)
    status_date = models.DateTimeField(blank=True, null=True)
    status_report = models.CharField(max_length=100, blank=True, null=True)
    handling_date = models.DateField(blank=True, null=True)
    handling_desc = models.TextField(blank=True, null=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    system = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inspecture_note'


class Location(models.Model):
    location_id = models.CharField(primary_key=True, max_length=50)
    location_name = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    province = models.CharField(max_length=100, blank=True, null=True)
    otban_id = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'location'


class Module(models.Model):
    device_id = models.CharField(primary_key=True, max_length=20)
    module_id = models.CharField(max_length=20)
    module_desc = models.CharField(max_length=50, blank=True, null=True)
    status_module = models.CharField(max_length=255, blank=True, null=True)
    vendor_id = models.CharField(max_length=10, blank=True, null=True)
    error_code = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'module'
        unique_together = (('device_id', 'module_id'),)


class PortDetail(models.Model):
    port_number = models.CharField(primary_key=True, max_length=10)
    port_name = models.TextField(blank=True, null=True)
    latitude = models.TextField(blank=True, null=True)
    longitude = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'port_detail'


class RmmsOid(models.Model):
    oids = models.CharField(max_length=100, blank=True, null=True)
    device_id = models.CharField(primary_key=True, max_length=20)
    modul_id = models.CharField(max_length=20)
    status_id = models.CharField(max_length=255, blank=True, null=True)
    date_time = models.DateTimeField(blank=True, null=True)
    error_code = models.IntegerField(blank=True, null=True)
    flag_status = models.CharField(max_length=20, blank=True, null=True)
    flag_broadcast = models.CharField(max_length=20, blank=True, null=True)
    flag_rlu = models.CharField(max_length=20, blank=True, null=True)
    flag_ds_status = models.CharField(max_length=20, blank=True, null=True)
    flag_fix_value = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rmms_oid'
        unique_together = (('device_id', 'modul_id'),)


class Status(models.Model):
    status_id = models.CharField(primary_key=True, max_length=10)
    status_desc = models.CharField(max_length=100, blank=True, null=True)
    color = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'status'


class StatusReport(models.Model):
    case_id = models.AutoField(primary_key=True)
    device_id = models.CharField(max_length=20, blank=True, null=True)
    modul_id = models.CharField(max_length=20, blank=True, null=True)
    status_id = models.IntegerField(blank=True, null=True)
    rdate_time = models.DateTimeField(blank=True, null=True)
    error_code = models.IntegerField(blank=True, null=True)
    vendor_id = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'status_report'


class System(models.Model):
    system_id = models.CharField(primary_key=True, max_length=10)
    system_desc = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system'


class UserActivityLog(models.Model):

    class Meta:
        managed = False
        db_table = 'user_activity_log'


class UserLogin(models.Model):
    id = models.DecimalField(primary_key=True, max_digits=65535, decimal_places=65535)
    tokenid = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    id_user_rule = models.CharField(max_length=50, blank=True, null=True)
    ctreated_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    inspector_enable = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_login'


class UserMenu(models.Model):
    id_menu = models.DecimalField(primary_key=True, max_digits=65535, decimal_places=65535)
    label_menu = models.CharField(max_length=100, blank=True, null=True)
    value_link = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_menu'


class UserRule(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    rulename = models.CharField(max_length=50, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_rule'
